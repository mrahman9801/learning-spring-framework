package com.example.learningspringframework;

import com.example.learningspringframework.game.GamerRunner;
import com.example.learningspringframework.game.MarioGame;
import com.example.learningspringframework.game.PacmanGame;
import com.example.learningspringframework.game.StreetFighterGame;

public class Gaming01ApplicationJava {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		var game = new MarioGame();
//		var game = new StreetFighterGame();
		var game = new PacmanGame();
		
		var gameRunner = new GamerRunner(game);
		
		gameRunner.run();
	}

}
