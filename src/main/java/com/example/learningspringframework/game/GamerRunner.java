package com.example.learningspringframework.game;

public class GamerRunner {
	
	private GamingConsole game;
	
	public GamerRunner(GamingConsole game) {
		this.game = game;
	}

	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Running game: " + game);
		game.up();
		game.right();
		game.right();
		game.down();
	}
}
