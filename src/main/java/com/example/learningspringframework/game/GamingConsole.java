package com.example.learningspringframework.game;

public interface GamingConsole {
	void up();
	void down();
	void right();
	void left();
}
