package com.example.learningspringframework.game;

public class StreetFighterGame implements GamingConsole {

	public void up() {
		System.out.println("Jump!");
	}
	
	public void down() {
		System.out.println("Dodge!");
	}
	
	public void right() {
		System.out.println("Moving right!");
	}
	
	public void left() {
		System.out.println("Moving left!");
	}
	
	public void lightAttack() {
		System.out.println("Punch!");
	}
	
	public void heavyAttack() {
		System.out.println("SMASH!!!");
	}
	
	public void block() {
		System.out.println("Attack blocked!");
	}
}
