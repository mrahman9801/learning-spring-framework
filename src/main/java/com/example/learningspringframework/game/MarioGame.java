package com.example.learningspringframework.game;

public class MarioGame implements GamingConsole {

	public void up() {
		System.out.println("Jump!");
	}
	
	public void down() {
		System.out.println("Going down into hole!");
	}
	
	public void right() {
		System.out.println("Going forward!");
	}
	
	public void left() {
		System.out.println("Going back!");
	}
}
