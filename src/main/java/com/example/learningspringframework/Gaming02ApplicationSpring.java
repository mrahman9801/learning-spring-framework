package com.example.learningspringframework;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.example.learningspringframework.game.GamerRunner;
import com.example.learningspringframework.game.MarioGame;
import com.example.learningspringframework.game.PacmanGame;
import com.example.learningspringframework.game.StreetFighterGame;

public class Gaming02ApplicationSpring {

	public static void main(String[] args) {
		// Launch a spring context
		var context = new AnnotationConfigApplicationContext(NewConfiguration.class);
		// configure what spring will manage - @Configuration
		System.out.println(context.getBean("name"));
		
	}

}
